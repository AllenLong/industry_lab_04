package ictgradschool.industry.lab04.ex05;

import ictgradschool.Keyboard;

/**
 * A game of Rock, Paper Scissors
 */
public class RockPaperScissors {

    public static final int ROCK = 1;
    // TODO Make similar constants for PAPER and SCISSORS, to improve readability of your code.
    public static final int SCISSORS = 2;
    public static final int PAPER = 3;
    public static final int QUIT = 4;

    public void start() {

        // TODO Write your code here which calls your other methods in order to play the game. Implement this
        // as detailed in the exercise sheet.
        System.out.print("Hi! What's your name? ");
        String name = Keyboard.readInput();

        while (true) {
            printInputOptions();
            int choice = Integer.parseInt(Keyboard.readInput());

            if (choice == QUIT) {
                System.out.println("Goodbye "+ name + ". Thanks for playing :)");
                return;
            }
            displayPlayerChoice(name,choice);
            int comChoice = (int)(Math.random()*3+1);
            displayComputerChoice(name,comChoice);
            int playerChoice = choice;
            int computerChoice = comChoice;
            userWins(playerChoice,computerChoice);
            if(userWins(playerChoice,computerChoice)){
                System.out.println(name + " win "+ " because "+getResultString(playerChoice,computerChoice));
            } else if (playerChoice == computerChoice) {
                System.out.println("No one wins, " + getResultString(playerChoice,computerChoice));
            } else {
                System.out.println("The computer win "+ " because "+getResultString(playerChoice,computerChoice));
            }



            //Who won?
            //Get the result string
            // Assemble to produce the output

           /*
            int comChoice = 0;
            displayPlayerChoice(name, choice);
            int playerchoice = choice;
            int computerChoice = comChoice;
            userWins(playerchoice, computerChoice);
            getResultString(playerchoice, computerChoice);*/
        }
    }

    private void printInputOptions() {
        System.out.println("1. Rock");
        System.out.println("2. Scissors");
        System.out.println("3. Paper");
        System.out.println("4. Quit");
        System.out.print("Enter choice:");
    }

    public void displayComputerChoice(String name, int comChoice){
        String strChoice = "";
        name = "Computer";
        switch (comChoice) {
            case ROCK:
                strChoice = "Rock";
                break;
            case SCISSORS:
                strChoice = "Scissors";
                break;
            case PAPER:
                strChoice = "Paper";
                break;
        }
        System.out.println(name + " chose " + comChoice);
    }


    public void displayPlayerChoice(String name, int choice) {
        // TODO This method should print out a message stating that someone chose a particular thing (rock, paper or scissors)
        String strChoice = "";

        switch (choice) {
            case ROCK:
                strChoice = "Rock";
                break;
            case SCISSORS:
                strChoice = "Scissors";
                break;
            case PAPER:
                strChoice = "Paper";
                break;
        }

        System.out.println(name + " chose " + choice);
    }

    public boolean userWins(int playerChoice, int computerChoice) {
        // TODO Determine who wins and return true if the player won, false otherwise.
        if (playerChoice == computerChoice) {
            return false;
        }

        if (playerChoice == ROCK) {
            if (computerChoice == SCISSORS) {
                return true;
            }
            return false;
        }
        if (playerChoice == SCISSORS) {
            if (computerChoice == ROCK) {
                return false;
            }
            return true;
        }

        if (playerChoice == PAPER) {
            if (computerChoice == ROCK) {
                return true;
            }
            return false;
        }

        return true;
    }


    public String getResultString(int playerChoice, int computerChoice) {

        final String PAPER_WINS = "paper covers rock";
        final String ROCK_WINS = "rock smashes scissors";
        final String SCISSORS_WINS = "scissors cut paper";
        final String TIE = " you chose the same as the computer";

        // TODO Return one of the above messages depending on what playerChoice and computerChoice are.

        if (playerChoice == computerChoice) {
            return TIE;
        }

        switch (playerChoice) {
            case ROCK:
                if (computerChoice == PAPER) {
                    return PAPER_WINS;
                }

                return ROCK_WINS;
            case PAPER:
                if (computerChoice == SCISSORS) {
                    return SCISSORS_WINS;
                }

                return PAPER_WINS;
            case SCISSORS:
                if (computerChoice == ROCK) {
                    return ROCK_WINS;
                }

                return SCISSORS_WINS;
        }

        return "Something is seriously wrong here";
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

    }
}
