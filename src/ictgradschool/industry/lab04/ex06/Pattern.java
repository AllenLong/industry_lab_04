package ictgradschool.industry.lab04.ex06;

/**
 * Created by yl525 on 7/03/2017.
 */
public class Pattern {
    private int num;
    private char pat;

    public Pattern(int num,char pat){
        this.num=num;
        this.pat=pat;
    }
    public void setNumberOfCharacters(int num){
        this.num = num;
    }
    public int getNumberOfCharacters(){
        return num;
    }
public String toString(){
    String line = "";
    for(int i = 0; i<=num;i++){
        line+=pat;
    }
    return line;
}
}